<?php
/**
 * @copyright <a href="http://juandon.rf.gd/" target="_blank">juandon</a>
 * @package   Core
 * @license   https://opensource.org/licenses/mit-license.php
 * @version   {$Id}
 */

namespace juandon\Core;

use \InvalidArgumentException;
use \RuntimeException;
use \juandon\Core\RegistryExtended;
use \juandon\Core\Event\Manager;
use \juandon\Core\Log\Factory;
use \juandon\Utilities\Config\Ini;

/**
 * Core initialization.
 *
 * @package Core
 */
class Bootstrap
{
    /**
     * Exception code
     */
    const EX_CANNOT_PARSE_CONFIG = 1;

    /**
     * Exception code
     */
    const EX_INVALID_ARG         = 2;


    /**
     * Exception code prefix
     */
    const EX_CODE_PREFIX = 0x1000;

    /**
     * Initializes environment by config file path.
     *
     * @param  string $path
     * @return void
     * @throws RuntimeException  In case of impossibility of parsings config file
     */
    public static function initByPath($path)
    {
        $config = Ini::parseFile($path, TRUE);
        if (!is_array($config)) {
            throw new RuntimeException(
                sprintf("Cannot parse config file \"%s\%", $path),
                self::EX_CODE_PREFIX | self::EX_CANNOT_PARSE_CONFIG
            );
        }
        self::initByArray($config);
    }

    /**
    * Initializes environment by config array.
    *
    * @param  array $config
    * @return void
    * @throws InvalidArgumentException
    */
    public static function initByArray(array $config)
    {
        if (!is_array($config)) {
            throw new InvalidArgumentException(
                sprintf("Passed argement isn't array (%s)", gettype($config)),
                self::EX_CODE_PREFIX | self::EX_INVALID_ARG
            );
        }
        $registry = RegistryExtended::_getInstance($config);
        $evtManager = new Manager;
        $evtManager->setDebug($registry->get('core/event/debug', FALSE));
        Factory::run($registry, $evtManager);
        $registry->set('core/event/manager', $evtManager);
    }
}
