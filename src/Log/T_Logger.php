<?php
/**
 * @copyright  <a href="http://juandon.rf.gd/" target="_blank">juandon</a>
 * @package    Core
 * @subpackage Log
 * @license    https://opensource.org/licenses/mit-license.php
 * @version    {$Id}
 */

namespace juandon\Core\Log;

use \juandon\Core\Event\Args;

/**
 * "config.ini.php" file:
 * <code>
 * ;<?php die; __halt_compiler();
 * [core]
 *
 * ; By default: Off
 * event[debug] = On
 *
 * ; By default: "StdOut"
 * ;log[method] = "StdOut"
 *
 * ; By default: E_ERROR | E_WARNING
 * log[level] = E_ERROR | E_WARNING | E_NOTICE
 *
 * ;log.source[] = "*" ; means all log sources
 * log.source[] = "Foo::someMethod()"
 * </code>
 * PHP-executable:
 * <code>
 * <?php
 *
 * require_once "/path/to/vendor/autoload.php";
 *
 * use \juandon\Core\Bootstrap;
 * use \juandon\Core\RegistryExtended;
 * use \juandon\Core\Log\T_Logger;
 *
 * Bootstrap::initByPath("./config.ini.php");
 *
 * class Foo
 * {
 *     use T_Logger;
 *
 *     public function __construct()
 *     {
 *         $this->evtManager = RegistryExtended::_getInstance()->get('core/event/manager');
 *     }
 *
 *     public function someMethod()
 *     {
 *         $this->log("notice",  'Foo::someMethod()', E_NOTICE);
 *         $this->log("warning", 'Foo::someMethod()', E_WARNING);
 *         $this->log("error",   'Foo::someMethod()', E_ERROR);
 *     }
 *
 *     public function otherMethod()
 *     {
 *         $this->log("notice from other method",  'Foo::otherMethod()');
 *     }
 * }
 *
 * $registry = RegistryExtended::_getInstance();
 * $foo = new Foo;
 * $foo->someMethod();
 * $foo->otherMethod();
 * $registry->delete('core/log/level');
 * echo PHP_EOL;
 * $foo->someMethod();
 * $source = $registry->get('core/log/source');
 * $source[] = "*";
 * $registry->set('core/log/source', $source);
 * echo PHP_EOL;
 * $foo->otherMethod();
 * </code>
 * Output:
 * <code>
 * [ 2017-07-15 19:59:08 ][ note ][ Foo::someMethod() ] ~ notice
 * [ 2017-07-15 19:59:08 ][ WARN ][ Foo::someMethod() ] ~ warning
 * [ 2017-07-15 19:59:08 ][ ERR  ][ Foo::someMethod() ] ~ error
 *
 * [ 2017-07-15 19:59:08 ][ WARN ][ Foo::someMethod() ] ~ warning
 * [ 2017-07-15 19:59:08 ][ ERR  ][ Foo::someMethod() ] ~ error
 *
 * [ 2017-07-15 19:59:08 ][ WARN ][ Foo::otherMethod() ] ~ warning from other method
 * </code>
 *
 * @package    Core
 * @subpackage Log
 */
trait T_Logger
{
    /**
     * @var \juandon\Core\Event\Manager
     */
    protected $evtManager;

    /**
     * Fires event to log message.
     *
     * @param  string $message
     * @param  string $source
     * @param  int    $level
     * @return void
     */
    protected function log($message, $source = NULL, $level = E_NOTICE)
    {
        if (is_null($source)) {
            $source = "MISSED SOURCE";
        }
        $args = new Args([
            'message' => $message,
            'source'  => $source,
            'level'   => $level,
        ]);
        $this->evtManager->fire(':log:', $args);
    }
}
