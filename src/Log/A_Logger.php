<?php
/**
 * @copyright  <a href="http://juandon.rf.gd/" target="_blank">juandon</a>
 * @package    Core
 * @subpackage Log
 * @license    https://opensource.org/licenses/mit-license.php
 * @version    {$Id}
 */

namespace juandon\Core\Log;

use \juandon\Core\Event\Manager;
use \juandon\Core\Event\Args;

/**
 * Logger abstract class.
 *
 * See {@see \juandon\Core\Log\T_Logger} for usage.
 *
 * @package    Core
 * @subpackage Log
 */
abstract class A_Logger
{
    /**
     * @var array
     */
    protected $levelToString = [
        E_NOTICE  => 'note',
        E_WARNING => 'WARN',
        E_ERROR   => 'ERR ',
    ];

    /**
     * @var Config
     */
    protected $registry;

    /**
     * @var Manager
     */
    protected $evtManager;

    /**
     * @param \juandon\Core\RegistryExtended $registry
     * @param \juandon\Core\Event\Manager    $evtManager
     */
    public function __construct(
        \juandon\Core\RegistryExtended $registry,
        \juandon\Core\Event\Manager    $evtManager
    )
    {
        $this->registry = $registry;
        $this->init($evtManager);
    }

    /**
     * Logging handler.
     *
     * @param  string $name
     * @param  Args   $args
     * @return void
     */
    public function handler($name, Args $args)
    {
        if (
            !$this->checkLevel($args->get('level')) ||
            !$this->checkSource($args->get('source'))
        ) {
            return;
        }

        switch ($name) {
            /*
            case ':log:':
                $this->log($args);
                break; // case 'log'

            */
            case ':onAddHandler:':
                $args->set('message', sprintf(
                    "Adding handler %s, %s",
                    $this->handlerToString($args->get('handler'), $args->get('name')),
                    $args->get('added', FALSE) ? 'successfully' : 'failed'
                ));
                break; // case ':onAddHandler:'

            case ':onEventStart:':
                $args->set('message', sprintf(
                    "{ '%s' (%s), args [%s]",
                    $args->get('name'),
                    $args->get('uid'),
                    implode(', ', array_keys($args->get()))
                ));
                break; // case ':onEventStart:'

            case ':onHandlerFound:':
                $args->set('message', sprintf(
                    "Handler %s found",
                    $this->handlerToString($args->get('handler'))
                ));
                break; // case ':onAddHandler:'

            case ':onEventEnd:':
                $args->set('message', sprintf(
                    "} '%s' (%s), args [%s]",
                    $args->get('name'),
                    $args->get('uid'),
                    implode(', ', array_keys($args->get()))
                ));
                break; // case ':onEventEnd:'

            case ':onDropHandlers:':
                $args->set('message', sprintf(
                    "Dropping handler %s",
                    $this->handlerToString($args->get('handler'), $args->get('name'))
                ));
                break; // case ':onDropHandlers:'

            case ':onDisableHandler:':
                $args->set('message', sprintf(
                    "Disabling handler %s",
                    $this->handlerToString($args->get('handler'), $args->get('name'))
                ));
                break; // case ':onDropHandlers:'

            case ':onEnableHandler:':
                $args->set('message', sprintf(
                    "Enabling handler %s",
                    $this->handlerToString($args->get('handler'), $args->get('name'))
                ));
                break; // case ':onDropHandlers:'
        }
        $this->log($args);
    }

    /**
     * Logger.
     *
     * @param  Args   $args
     * @return void
     */
    protected abstract function log(Args $args);

    /**
     * Inializes event handler to log messages.
     *
     * @param  Manager $evtManager
     * @return void
     */
    protected function init(Manager $evtManager)
    {
        $this->evtManager = $evtManager;
        $events = $this->evtManager->getDebugEvents();
        foreach ($events as $event) {
            $this->evtManager->addHandler($event, [$this, 'handler']);
        }
    }

    /**
     * Returns TRUE if passed source published in config file.
     *
     * @param  string $source
     * @return bool
     */
    protected function checkSource($source)
    {
        $sources = $this->registry->get('core/log/source', []);
        $result  = in_array($source, $sources) || in_array('*', $sources);

        return $result;
    }

    /**
     * Returns TRUE if passed level published in config file.
     *
     * @param  int $level
     * @return bool
     */
    protected function checkLevel($level)
    {
        $default = E_ERROR | E_WARNING;
        $cfgLevel = $this->registry->get('core/log/level', $default);
        if (is_numeric($cfgLevel)) {
            $cfgLevel = (int)$cfgLevel;
        } else if (is_string($cfgLevel) && preg_match('/^[A-Z_&|^ ]+$/', $cfgLevel)) {
            $cfgLevel = eval("return {$cfgLevel};");
        } else {
            $cfgLevel = $default;
        }
        $result = (bool)($cfgLevel & $level);

        return $result;
    }

    /**
    * Returns stringified event handler.
    *
    * @param  callable $handler
    * @param  string   $name
    * @return string
    */
    protected function handlerToString($handler, $name = '')
    {
        $result = '';
        if (is_array($handler)) {
            if (is_object($handler[0])) {
                $class = get_class($handler[0]);
                $call = '->';

            } else {
                $class = $handler[0];
                $call = '::';
            }
            $result = "{$class}{$call}{$handler[1]}";
        } else {
            $result = 'function' . ('' != $handler ? " {$handler}" : '');
        }
        $result .= "({$name})";

        return $result;
    }
}
