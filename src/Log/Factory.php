<?php
/**
 * @copyright  <a href="http://juandon.rf.gd/" target="_blank">juandon</a>
 * @package    Core
 * @subpackage Log
 * @license    https://opensource.org/licenses/mit-license.php
 * @version    {$Id}
 */

namespace juandon\Core\Log;

/**
 * See {@see \juandon\Core\Log\T_Logger} for usage.
 *
 * @package    Core
 * @subpackage Log
 */
class Factory
{
    /**
     * Default logger
     *
     * @var string
     */
    protected static $default = 'StdOut';

    /**
     * Factory.
     *
     * @param  \juandon\Core\RegistryExtended $registry
     * @param  \juandon\Core\Event\Manager    $evtManager
     * @return void
     */
    public static function run(
        \juandon\Core\RegistryExtended $registry,
        \juandon\Core\Event\Manager    $evtManager
    )
    {
        $class = '\\juandon\\Core\\Log\\Method\\' . $registry->get('core/method', self::$default);
        new $class($registry, $evtManager);
    }
}
