<?php
/**
 * @copyright  <a href="http://juandon.rf.gd/" target="_blank">juandon</a>
 * @package    Core
 * @subpackage Log
 * @license    https://opensource.org/licenses/mit-license.php
 * @version    {$Id}
 */

namespace juandon\Core\Log\Method;

use \juandon\Core\Log\A_Logger;
use \juandon\Core\Event\Args;

/**
 * StdOut logger.
 *
 * See {@see \juandon\Core\Log\T_Logger} for usage.
 *
 * @package    Core
 * @subpackage Log
 * @todo       Log environment (configurable)
 */
class StdOut extends A_Logger
{
    /**
     * @inheritdoc
     */
    protected function log(Args $args)
    {
        echo sprintf(
            "[ %s ][ %s ][ %s ] ~ %s%s",
            date('Y-m-d H:i:s'),
            $this->levelToString[$args->get('level')],
            $args->get('source'),
            $args->get('message'),
            PHP_EOL
        );
    }
}
