<?php
/**
 * @copyright  <a href="http://juandon.rf.gd/" target="_blank">juandon</a>
 * @package    Core
 * @subpackage Registry
 * @license    https://opensource.org/licenses/mit-license.php
 * @version    {$Id}
 */

namespace juandon\Core;

use \RuntimeException;

/**
 * Static and non-static simple non-recursive registry functionality.
 *
 * PHP-executable:
 * <code>
 * <?php
 *
 * require_once "/path/to/vendor/autoload.php";
 *
 * use \juandon\Core\Registry;
 *
 * $registry = new Registry([
 *     'key_1' => "value_1",
 *     'key_2' => "value_2",
 * ]);
 * var_dump($registry->exists('key_1'));
 * var_dump($registry->exists('key_3'));
 * $registry->set('key_3', "value_3");
 * var_dump($registry->get());
 * </code>
 * Output:
 * <code>
 * bool(true)
 * bool(false)
 * array(3) {
 *   ["key_1"]=>
 *   string(7) "value_1"
 *   ["key_2"]=>
 *   string(7) "value_2"
 *   ["key_3"]=>
 *   string(7) "value_2"
 * }
 * </code>
 *
 * @package    Core
 * @subpackage Registry
 */
class Registry
{
    /**
     * Allowed action
     *
     * @see self::__construct()
     */
    const ACTION_CREATE   = 0x0001;

    /**
     * Allowed action
     *
     * @see self::__construct()
     */
    const ACTION_DELETE   = 0x0002;

    /**
     * Allowed action
     *
     * @see self::__construct()
     */
    const ACTION_MODIFY   = 0x0004;

    /**
     * Allowed action
     *
     * @see self::__construct()
     */
    const ACTION_OVERRIDE = 0x0008;

    /**
     * Allowed action
     *
     * @see self::__construct()
     */
    const ACTION_ALL      = 0x000F;

    /**
     * Exception code prefix
     *
     * @see self::checkPermissions()
     */
    const EX_CODE_PREFIX  = 0x2000;

    /**
     * Global registry
     *
     * @var self
     */
    protected static $instance;

    /**
     * @var array
     */
    protected $scope;

    /**
     * @var int
     */
    protected $options;

    /**
     * Returns registry static instance.
     *
     * @param  array $scope
     * @param  int   $options
     * @return self
     */
    public static function _getInstance(
        array $scope = [],
        $options = self::ACTION_ALL
    )
    {
        if (!is_object(self::$instance)) {
            self::$instance = new static($scope, $options);
        }

        return self::$instance;
    }

    /**
     * @see    self::set()
     * @return void
     */
    public static function _set($key, $value)
    {
        self::_getInstance()->set($key, $value);
    }

    /**
     * @see    self::exists()
     * @return bool
     */
    public static function _exists($key)
    {
        return self::_getInstance()->exists($key);
    }

    /**
     * @see    self::isEmpty()
     * @return bool
     */
    public static function _isEmpty($key)
    {
        return self::_getInstance()->isEmpty($key);
    }

    /**
     * @see    self::get()
     * @return mixed
     */
    public static function _get($key = NULL, $default = NULL)
    {
        return self::_getInstance()->get($key, $default);
    }

    /**
     * @see    self::delete()
     * @return void
     */
    public static function _delete($key)
    {
        self::_getInstance()->delete($key);
    }

    /**
     * @see    self::override()
     * @return void
     */
    public static function _override(array $scope)
    {
        self::_getInstance()->override($scope);
    }

    /**
     * @param  array $scope
     * @param  int   $options
     */
    public function __construct(
        array $scope = [],
        $options = self::ACTION_ALL
    )
    {
        $this->scope   = (array)$scope;
        $this->options = (int)$options;
    }

    /**
     * Sets scope value.
     *
     * @param  string $key
     * @param  mixed  $value
     * @return void
     */
    public function set($key, $value)
    {
        $this->checkPermissions(
            array_key_exists($key, $this->scope)
                ? self::ACTION_MODIFY
                : self::ACTION_CREATE
        );
        $this->scope[$key] = $value;
    }

    /**
     * Returns TRUE if scope exists, FALSE otherwise.
     *
     * @param  string $key
     * @return bool
     */
    public function exists($key)
    {
        $result = array_key_exists($key, $this->scope);

        return $result;
    }

    /**
     * Returns TRUE if scope value is empty, FALSE otherwise.
     *
     * @param  string $key
     * @return bool
     * @see    http://php.net/manual/en/function.empty.php
     */
    public function isEmpty($key)
    {
        $result = empty($this->scope[$key]);

        return $result;
    }

    /**
     * Returns scope value.
     *
     * @param  string $key     If not passed, whole scope will be returned
     * @param  mixed  $default
     * @return mixed
     */
    public function get($key = NULL, $default = NULL)
    {
        if (is_null($key)) {
            $result = $this->scope;
        } else {
            $result =
                is_array($this->scope) && array_key_exists($key, $this->scope)
                    ? $this->scope[$key]
                    : $default;
        }

        return $result;
    }

    /**
     * Deletes scope value.
     *
     * @param  string $key
     * @return void
     */
    public function delete($key)
    {
        $this->checkPermissions(self::ACTION_DELETE);
        unset($this->scope[$key]);
    }

    /**
     * Overrides scope.
     *
     * @param  array $scope
     * @return void
     * @throws
     */
    public function override(array $scope)
    {
        $this->checkPermissions(self::ACTION_OVERRIDE);
        $this->scope = $scope;
    }

    /**
     * Chceck action permissions.
     *
     * @param  int $action
     * @return void
     * @throws RuntimeException  In case of fail
     */
    protected function checkPermissions($action)
    {
        if (!($action & $this->options)) {
            throw new RuntimeException(
                sprintf(
                    "No have permissions for %s",
                    Friendlification::getConstNameByValue(__CLASS__, $action)
                ),
                self::EX_CODE_PREFIX | $action
            );
        }
    }
}
