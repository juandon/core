<?php
/**
 * @copyright  <a href="http://juandon.rf.gd/" target="_blank">juandon</a>
 * @package    Core
 * @subpackage Event
 * @license    https://opensource.org/licenses/mit-license.php
 * @version    {$Id}
 */

namespace juandon\Core\Event;

use \InvalidArgumentException;
use \RuntimeException;
use \juandon\Utilities\Arrays;

/**
 * Event manager.
 *
 * @package    Core
 * @subpackage Event
 */
class Manager
{
    /**
     * Minimum event handler priority
     *
     * @var int
     * @see Manager::addHandler()
     */
    const PRIORITY_MIN = 0;

    /**
     * Low event handler priority
     *
     * @var int
     * @see Manager::addHandler()
     */
    const PRIORITY_LOW = 25;

    /**
     * Default event handler priority
     *
     * @var int
     * @see Manager::addHandler()
     */
    const PRIORITY_DEFAULT = 50;

    /**
     * High event handler priority
     *
     * @var int
     * @see Manager::addHandler()
     */
    const PRIORITY_HIGH = 75;

    /**
     * Minimum event handler priority
     *
     * @var int
     * @see Manager::addHandler()
     */
    const PRIORITY_MAX = 99;

    /**
     * Array of hidden event debugger events
     *
     * @var array
     */
    protected static $debugEvents =
        [
            ':log:',
            ':onAddHandler:',
            ':onEventStart:',
            ':onHandlerFound:',
            ':onEventEnd:',
            ':onDropHandlers:',
            ':onDisableHandler:',
            ':onEnableHandler:',
        ];

    /**
     * Event handlers
     *
     * @var array
     */
    protected $handlers = [];

    /**
     * Contains ordered by priority flags for each event name
     *
     * @var array
     */
    protected $ordered = [];

    /**
     * Contains disabled events.
     *
     * @var array
     */
    protected $disabledEvents = [];

    /**
     * Contains fired events names && target module name to avoid recurring firing during its execution
     *
     * @var array
     */
    protected $firedEvents = [];

    /**
     * Enables {@link Manager::$debugEvents} events
     *
     * @var bool
     */
    protected $debug = FALSE;

    /**
     * Adds event handler.
     *
     * To break event handling handler must set $args[':break:'] to TRUE.<br /><br />
     *
     * Example:
     * <code>
     * // @param  string $name  Event name
     * // @param  Args   $args  Event arguments
     * // @return void
     * function eventHandler($name, Args $args) {
     *     $args->set('varName', 'varValue');
     *     $args->set(':break:', TRUE);
     * }
     * </code>
     *
     * @param  string   $name      Event name
     * @param  callback $handler   Event handler callback
     * @param  int      $priority  Event priority:
     *                             Manager::PRIORITY_LOW, Manager::PRIORITY_DEFAULT or Manager::PRIORITY_HIGH
     * @return void
     * @throws InvalidArgumentException  In case of invalid priority.
     */
    public function addHandler($name, $handler, $priority = self::PRIORITY_DEFAULT)
    {
        $priority = (int)$priority;
        if ($priority < self::PRIORITY_MIN || $priority > self::PRIORITY_MAX) {
            throw new InvalidArgumentException("Invalid event priority");
        }
        $key = '';
        if (is_array($handler)) {
            $key .= (is_object($handler[0]) ? get_class($handler[0]) : $handler[0]) . '|' . $handler[1];
        } else {
            $key .= $handler;
        }
        $addHandler = TRUE;
        if (empty($this->handlers[$name])) {
            $this->handlers[$name] = [];
        } else if (isset($this->handlers[$name][$key])) {
            $addHandler = FALSE;
        }
        if ($addHandler) {
            $this->handlers[$name][$key] = [$priority, $handler];
            $this->ordered[$name] = FALSE;
        }
        if ($this->debug && !in_array($name, self::$debugEvents)) {
            $debugArgs = new Args([
                'name'    => $name,
                'handler' => $handler,
                'added'   => $addHandler,
                'source'  => 'core:event:debug',
                'level'   => $addHandler ? E_NOTICE : E_WARNING,
            ]);
            $this->fire(':onAddHandler:', $debugArgs);
        }
    }

    /**
     * Drops event handlers.
     *
     * Example:
     * <code>
     * // drop all 'some_event_name' event handlers
     * Manager::dropHandler('some_event_name');
     *
     * // drop all 'some_event_name' event handlers processing by $object methods only
     * Manager::dropHandler('some_event_name', $object);
     * </code>
     *
     * @param  string $name     Event name
     * @param  mixed  $handler  Hadler or its part ctiteria
     * @return void
     * @throws InvalidArgumentException  In case of dropping debug event.
     */
    public function dropHandlers($name = '', $handler = NULL)
    {
        if (
            in_array($name, self::$debugEvents) &&
            (is_null($handler) || ($handler[0] == $this))
        ) {
            throw new InvalidArgumentException("Cannot drop debug event");
        }
        if ($this->debug) {
            $debugArgs = new Args([
                'name'    => $name,
                'handler' => $handler,
                'source'  => 'core:event:debug',
                'level'   => E_NOTICE,
            ]);
            $this->fire(':onDropHandlers:', $debugArgs);
        }
        $this->runByHandler($name, $handler, [$this, 'cleanupHandler']);
    }

    /**
     * Disable handler.
     *
     * @param  string $name  Event name
     * @return void
     * @throws InvalidArgumentException  In case of disabling debug event.
     */
    public function disableHandler($name)
    {
        if (
            in_array($name, self::$debugEvents) &&
            (is_null($handler) || ($handler[0] == $this))
        ) {
            throw new InvalidArgumentException("Cannot disable debug event");
        }
        if ($this->debug) {
            $debugArgs = new Args([
                'name'    => $name,
                'source'  => 'core:event:debug',
                'level'   => E_NOTICE,
            ]);
            $this->fire(':onDisableHandler:', $debugArgs);
        }
        $this->disabledEvents[$name] = TRUE;
    }

    /**
     * Enable handler.
     *
     * @param  string $name  Event name
     * @return void
     */
    public function enableHandler($name)
    {
        if ($this->debug) {
            $debugArgs = new Args([
                'name'    => $name,
                'source'  => 'core:event:debug',
                'level'   => E_NOTICE,
            ]);
            $this->fire(':onEnableHandler:', $debugArgs);
        }
        unset($this->disabledEvents[$name]);
    }

    /**
     * Returns TRUE if there are any handlers for specified event.
     *
     * @param  string $name     Event name
     * @param  mixed  $handler  Hadler or its part ctiteria
     * @return bool
     */
    public function hasHandlers($name, $handler = NULL)
    {
        $args = new Args;
        $this->runByHandler($name, $handler, [$this, 'hasHandler'], $args);
        $result = $args->get('hasHandler');

        return $result;
    }

    /**
     * Returns handlers for passed event or all.
     *
     * @param  string $name  Event name
     * @return array
     */
    public function getHandlers($name = NULL)
    {
        $result = is_null($name) ? $this->handlers : $this->handlers[$name];

        return $result;
    }

    /**
     * Fires event.
     *
     * @param  string $name       Event name
     * @param  Args   $args       Event arguments
     * @param  bool   $fireAgain  Allow to fire event again
     * @return void
     * @throws RuntimeException  When invalid callback passed.
     */
    public function fire($name, Args $args, $fireAgain = FALSE)
    {
        /* ###
        if (!in_array($name, self::$debugEvents)) {
            echo "fired {$name}\n";
            $a = $args->get();
            unset($a['tokens']);
            print_r($a);
            unset($a);
        }
        */ ###
        if ($this->debug && !in_array($name, self::$debugEvents)) {
            $uid = uniqid('');
            $debugArgs = new Args([
                'uid'    => $uid,
                'name'   => $name,
                'args'   => $args,
                'source' => 'core:event:debug',
                'level'  => E_NOTICE,
            ]);
            $this->fire(':onEventStart:', $debugArgs);
            unset($debugArgs);
        }
        if (isset($this->handlers[$name]) && empty($this->disabledEvents[$name])) {
            if (!$fireAgain && isset($this->firedEvents[$name])) {
                throw new RuntimeException(
                    sprintf(
                        "Event '%s' is fired already",
                        $name
                    )
                );
            } else {
                $this->firedEvents[$name] = TRUE;
                if (!$this->ordered[$name]) {
                    Arrays::sortByCol(
                        $this->handlers[$name],
                        0,
                        SORT_NUMERIC,
                        SORT_DESC
                    );
                    $this->ordered[$name] = TRUE;
                }
                foreach (array_keys($this->handlers[$name]) as $index) {
                    if (empty($this->handlers[$name][$index][1])) {
                        // Targeted event, not for this target
                        continue;
                    }
                    // Call handler
                    $callback = $this->handlers[$name][$index][1];
                    if (!is_callable($callback)) {
                        unset($this->firedEvents[$name]);
                        if (is_array($callback)) {
                            $callback =
                                (
                                    is_object($callback[0])
                                        ? get_class($callback[0]) . '->'
                                        : $callback[0] . '::'
                                ) .
                                $callback[1];
                        } else {
                            $callback = sprintf("function %s", $callback);
                        }

                        throw new RuntimeException(
                            sprintf(
                                "Invalid event handler %s added to process '%s' event.",
                                $callback,
                                $name
                            )
                        );
                    }

                    if ($this->debug && !in_array($name, self::$debugEvents)) {
                        $debugArgs = new Args([
                            'handler' => $this->handlers[$name][$index][1],
                            'source'  => 'core:event:debug',
                            'level'   => E_NOTICE,
                        ]);
                        $this->fire(':onHandlerFound:', $debugArgs);
                    }
                    call_user_func(
                        $this->handlers[$name][$index][1],
                        $name,
                        $args
                    );
                    if (!$args->isEmpty(':break:')) {
                        break;
                    }
                }
                unset($this->firedEvents[$name]);
            }
        }
        if ($this->debug && !in_array($name, self::$debugEvents)) {
            $debugArgs = new Args([
                'uid'    => $uid,
                'name'   => $name,
                'args'   => $args,
                'source' => 'core:event:debug',
                'level'  => $args->get(':break:', FALSE) ? E_WARNING : E_NOTICE,
            ]);
            $this->fire(':onEventEnd:', $debugArgs);
        }
    }

    /**
     * Setup internal debugging.
     *
     * @param  bool $debug  Enable/disable flag
     * @return void
     */
    public function setDebug($debug)
    {
        $this->debug = (bool)$debug;
    }

    /**
     * Returns list of debug events.
     *
     * @return array
     */
    public function getDebugEvents()
    {
        return self::$debugEvents;
    }

    /**
     * Search handlers by name and callback and run task for its.
     *
     * @param  string   $name         Event name, empty string for all event names
     * @param  mixed    $handler      Event handler ot part of handler
     * @param  callback $task         Task to run
     * @param  Args     $args         Arguments passed to / retirn by task
     * @return Args
     * @throws InvalidArgumentException  When $task parameter isn't callback.
     * @see    self::dropHandler() code for usage exaple
     */
    protected function runByHandler($name, $handler, $task, Args $args = NULL)
    {
        if (!is_callable($task)) {
            throw new InvalidArgumentException(
                "Passed \$task parameter isn't callback"
            );
        }
        // Detect handler type
        if (is_object($handler)) {
            $handlerType = 'object';
        } else if (is_string($handler)) {
            $handlerType = function_exists($handler) ? 'function' : 'class';
        } else if (is_array($handler)) {
            $handlerType = 'callback';
        } else {
            $handlerType = '';
        }
        $names = $name === '' ? array_keys($this->handlers) : [$name];
        if (is_null($args)) {
            $args = new Args;
        }
        foreach ($names as $name) {
            if (empty($this->handlers[$name])) {
                // There aren't handlers for specified event name
                continue;
            }
            $indices = array_keys($this->handlers[$name]);
            switch($handlerType) {
                case 'object':
                case 'function':
                    // Run task for specified object / specified functions
                    foreach ($indices as $index) {
                        if (
                            is_array($this->handlers[$name][$index]) &&
                            $this->handlers[$name][$index][1][0] === $handler
                        ) {
                            call_user_func_array($task, [$args, $name, $index]);
                            if ($args->get(':break:')) {
                                return $args;
                            }
                        }
                    }
                    break; // case 'object', case 'function'

                case 'class':
                    // Run task for specified classes methods
                    foreach ($indices as $index) {
                        if (
                            is_array($this->handlers[$name][$index]) &&
                            (
                                $this->handlers[$name][$index][1][0] == $handler ||
                                get_class($this->handlers[$name][$index][1][0]) == $handler
                            )
                        ) {
                            call_user_func_array($task, [$args, $name, $index]);
                            if ($args->get(':break:')) {
                                return $args;
                            }
                        }
                    }
                    break; // case 'class'

                case 'callback':
                    // Run task for specified callbacks
                    foreach ($indices as $index) {
                        if (
                            is_array($this->handlers[$name][$index]) &&
                            (
                                (
                                    $this->handlers[$name][$index][1][0] == $handler[0] ||
                                    (
                                        is_object($this->handlers[$name][$index][1][0]) && is_object($handler[0])
                                            ? get_class($this->handlers[$name][$index][1][0]) == get_class($handler[0])
                                            : $this->handlers[$name][$index][1][0] == $handler[0]
                                    )
                                ) &&
                                $this->handlers[$name][$index][1][1] == $handler[1]
                            )
                        ) {
                            call_user_func_array($task, [$args, $name, $index]);
                            if ($args->get(':break:')) {
                                return $args;
                            }
                        }
                    }
                    break; // case 'callback'

                default:
                    call_user_func_array($task, [$args, $name, NULL]);
                    if ($args->get(':break:')) {
                        return $args;
                    }
            }
        }

        return $args;
    }

    /**
     * Cleanups handler.
     *
     * @param  Args   $args   Any arguments, can be used to return something
     * @param  string $name   Event name
     * @param  int    $index  Index in self::$handlers[$name] array or NULL
     *                        if $handler parameter not callback or its part
     * @return bool           TRUE to continue, FALSE to interrupt execution for passed name
     * @see    Manager::dropHandler()
     */
    protected function cleanupHandler(Args $args, $name, $index = NULL)
    {
        $clenupAllHandlers = is_null($index);
        if (!$clenupAllHandlers) {
            unset($this->handlers[$name][$index]);
            if (sizeof($this->handlers[$name])) {
                ksort($this->handlers[$name]);
                $this->ordered[$name] = FALSE;
            } else {
                $clenupAllHandlers = TRUE;
            }
        }
        if ($clenupAllHandlers) {
            // Cleanup all handlers with specified name
            unset($this->handlers[$name], $this->ordered[$name]);
        }

        return FALSE;
    }

    /**
     * Cleanups handler.
     *
     * @param  Args   $args   Call $args->get('hasHadler') to detect result
     * @param  string $name   Event name
     * @param  int    $index  Index in self::$handlers[$name] array or NULL
     *                        if $handler parameter not callback or its part
     * @return bool           TRUE to continue, FALSE to interrupt execution
     * @see    self::hasHandlers()
     */
    protected function hasHandler(Args $args, $name, $index = NULL)
    {
        $result = is_null($index) ? !empty($this->handlers[$name]) : TRUE;
        $args->set('hasHadler', $result);

        return $result;
    }
}
