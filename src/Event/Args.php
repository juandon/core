<?php
/**
 * @copyright  <a href="http://juandon.rf.gd/" target="_blank">juandon</a>
 * @package    Core
 * @subpackage Event
 * @license    https://opensource.org/licenses/mit-license.php
 * @version    {$Id}
 */

namespace juandon\Core\Event;

/**
 * Event arguments.
 *
 * @package    Core
 * @subpackage Event
 */
class Args
{
    /**
     * Arguments
     *
     * @var array
     */
    protected $args;

    /**
     * @param array $args  Initial arguments
     */
    public function __construct(array $args = [])
    {
        $this->args = $args;
    }

    /**
     * Sets argument value.
     *
     * @param  string $name
     * @param  mixed  $value
     * @return void
     */
    public function set($name, $value)
    {
        $this->args[$name] = $value;
    }

    /**
     * Returns TRUE if argument exists, FALSE otherwise.
     *
     * @param  string $name
     * @return bool
     */
    public function exists($name)
    {
        $result = array_key_exists($name, $this->args);

        return $result;
    }

    /**
     * Returns TRUE if argument value is empty, FALSE otherwise.
     *
     * @param  string $name
     * @return bool
     * @see    http://php.net/manual/en/function.empty.php
     */
    public function isEmpty($name)
    {
        $result = empty($this->args[$name]);

        return $result;
    }

    /**
     * Returns argument value.
     *
     * @param  string $name     If not passed, all arguments will be returned
     * @param  mixed  $default
     * @return mixed
     */
    public function get($name = NULL, $default = NULL)
    {
        if (is_null($name)) {
            $result = $this->args;
        } else {
            $result = $this->exists($name) ? $this->args[$name] : $default;
        }

        return $result;
    }

    /**
     * Returns all arguments names.
     *
     * @return array
     */
    public function getNames()
    {
        $result = array_keys($this->args);

        return $result;
    }

    /**
     * Deletes argument.
     *
     * @param  string $name
     * @return void
     */
    public function delete($name)
    {
        unset($this->args[$name]);
    }
}
