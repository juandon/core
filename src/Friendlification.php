<?php
/**
 * @copyright  <a href="http://juandon.rf.gd/" target="_blank">juandon</a>
 * @package    Core
 * @license    https://opensource.org/licenses/mit-license.php
 * @version    {$Id}
 */

namespace juandon\Core;

use ReflectionClass;

/**
 * "Friendlification" functionality.
 *
 * @package    Core
 */
class Friendlification
{
    /**
     * Class reflections cache
     *
     * @var arrray
     */
    protected static $reflections = [];

    /**
     * Returns class const name by its value.
     *
     * @param  string $class
     * @param  mixed  $value
     * @return {string|FALSE}
     */
    public static function getConstNameByValue($class, $value)
    {
        $consts = self::getReflection($class)->getConstants();
        $result = array_search($value, $consts);

        return $result;
    }

    /**
     * Returns reflection by class name.
     *
     * @param  string $class
     * @return ReflectionClass
     */
    protected static function getReflection($class)
    {
        if (!isset(self::$reflections[$class])) {
            self::$reflections[$class] = new ReflectionClass($class);
        }
        $result = self::$reflections[$class];

        return $result;
    }
}
