<?php
/**
 * @copyright  <a href="http://juandon.rf.gd/" target="_blank">juandon</a>
 * @package    Core
 * @subpackage Registry
 * @license    https://opensource.org/licenses/mit-license.php
 * @version    {$Id}
 */

namespace juandon\Core;

/**
 * Recursive registry functionality.
 *
 * PHP-executable:
 * <code>
 * <?php
 *
 * require_once "/path/to/vendor/autoload.php";
 *
 * use \juandon\Core\RegistryExtended;
 *
 * $registry = new RegistryExtended([
 *     'key_1' => "value_1",
 *     'key_2' => [
 *         'key_2_1' => "value_2_1",
 *         'key_2_2' => "value_2_2",
 *     ],
 * ]);
 * var_dump($registry->exists('key_1'));
 * var_dump($registry->exists('key_2/key_2_3'));
 * $registry->set('key_2/key_2_2/key_2_2_1', "value_2_2_1");
 * var_dump($registry->get());
 * </code>
 * Output:
 * <code>
 * bool(true)
 * bool(false)
 * array(2) {
 *   ["key_1"]=>
 *   string(7) "value_1"
 *   ["key_2"]=>
 *   array(2) {
 *     ["key_2_1"]=>
 *     string(9) "value_2_1"
 *     ["key_2_2"]=>
 *     array(1) {
 *       ["key_2_2_1"]=>
 *       string(11) "value_2_2_1"
 *     }
 *   }
 * }
 * </code>
 *
 * @package    Core
 * @subpackage Registry
 */
class RegistryExtended extends Registry
{
    /**
     * @var array
     */
    protected $fullScope;

    /**
     * Path delimiter
     *
     * @var string
     */
    protected $delimiter;

    /**
     * @param array  $scope
     * @param int    $options
     * @param string $delimiter
     */
    public function __construct(
        array $scope = [],
        $options = self::ACTION_ALL,
        $delimiter = '/'
    )
    {
        $this->fullScope = $scope;
        $this->delimiter = $delimiter;
        parent::__construct($scope, $options);
    }

    /**
     * @inheritdoc
     */
    public function set($key, $value)
    {
        $this->setScope($key, TRUE);
        parent::set($key, $value);
    }

    /**
     * @inheritdoc
     */
    public function exists($key)
    {
        $this->setScope($key);
        # var_dump($key);###
        $result = parent::exists($key);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function isEmpty($key)
    {
        $this->setScope($key);
        $result = parent::isEmpty($key);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function get($key = NULL, $default = NULL)
    {
        $this->setScope($key);
        $result = parent::get($key, $default);

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function delete($key)
    {
        $this->setScope($key);
        parent::delete($key);
    }

    /**
     * @param  string $key
     * @param  bool   $create
     * @return void
     */
    protected function setScope(&$key, $create = FALSE)
    {
        $this->scope = &$this->fullScope;
        if (FALSE === strpos($key, $this->delimiter)) {
            return;
        }
        $this->skipSettingScope = TRUE;
        $keys = explode($this->delimiter, $key);
        $lastName = array_pop($keys);
        $lastIndex = sizeof($keys) - 1;
        foreach ($keys as $index => $key) {
            if (!isset($this->scope[$key]) || !is_array($this->scope[$key])) {
                if ($create) {
                    $this->scope[$key] = [];
                } else if (!isset($this->scope[$key]) && $index == $lastIndex) {
                    return;
                }
            }
            $this->scope = &$this->scope[$key];
        }
        if (isset($lastName)) {
            $key = $lastName;
        }
    }
}
