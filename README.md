# Core
Containig registry, events, logging functionality.

## Installing
---
Add following code to your "composer.json" file
```json
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/juandon/core"
        }
    ],
    "require": {
        "juandon/core": "dev-develop"
    }
```
and run `composer update` or run `composer require juandon/core`.

## Usage

Add following code to your bootstrap file:
```php
use \juandon\Core\Bootstrap;

Bootstrap::initByPath("./config.ini.php");
```
"config.ini.php" file:
```ini
;<?php die; __halt_compiler();

[core]
; By default: Off
event[debug] = On

; By default: "StdOut"
;log[method] = "StdOut"

; By default: E_ERROR | E_WARNING
log[level] = E_ERROR | E_WARNING | E_NOTICE

;log.source[] = "*" ; means all log sources
log.source[] = "Foo::someMethod()"
```

## @todo
Unit tests.
